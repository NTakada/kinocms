-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Май 04 2020 г., 16:10
-- Версия сервера: 10.3.22-MariaDB-1:10.3.22+maria~bionic-log
-- Версия PHP: 7.3.17-1+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `media`
--

-- --------------------------------------------------------

--
-- Структура таблицы `fos_user`
--

CREATE TABLE `fos_user` (
  `id` int(11) NOT NULL,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `fos_user`
--

INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`) VALUES
(1, 'admin', 'admin', 'vlad5@ukr.net', 'vlad5@ukr.net', 1, NULL, '$2y$13$znNzYnRidc5l7/EfcPjMr.5RGLyf6n/e2fozkZ.lgKI/PLsEyGSR.', '2020-05-03 22:58:34', NULL, NULL, 'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}'),
(2, 'vlad', 'vlad', 'vlad-5@ukr.net', 'vlad-5@ukr.net', 1, NULL, '$2y$13$qtAhKH1XIGIkD3nlcN3PAOEv/JXGkMLzTVp63mygEcaw246SyHnwC', '2020-05-03 18:23:00', NULL, NULL, 'a:0:{}'),
(3, 'ira', 'ira', 'ira@ukr.net', 'ira@ukr.net', 1, NULL, '$2y$13$BGCuQjvA8q0fohABHeCboeM7nxyBgx/QzgGSED5nemlT2OxsVNcj2', '2020-05-03 19:14:22', NULL, NULL, 'a:0:{}'),
(4, 'ira5', 'ira5', 'ira-5@ukr.net', 'ira-5@ukr.net', 1, NULL, '$2y$13$YGbs58pUZmqvvCkHnDjJc.5FHunN/4y.MArTiY50i6YmLn93UXGsy', '2020-05-03 17:01:23', NULL, NULL, 'a:0:{}'),
(5, 'lana', 'lana', 'lana@i.ua', 'lana@i.ua', 1, NULL, '$2y$13$/vyicvhm/t7Zd.5.CnpXPef7zQLj3Kp8jwarfaylBWUUOfONXHJCK', '2020-05-03 19:15:16', NULL, NULL, 'a:0:{}');

-- --------------------------------------------------------

--
-- Структура таблицы `migration_versions`
--

CREATE TABLE `migration_versions` (
  `version` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `migration_versions`
--

INSERT INTO `migration_versions` (`version`) VALUES
('20200503063903');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `fos_user`
--
ALTER TABLE `fos_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`),
  ADD UNIQUE KEY `UNIQ_957A6479C05FB297` (`confirmation_token`);

--
-- Индексы таблицы `migration_versions`
--
ALTER TABLE `migration_versions`
  ADD PRIMARY KEY (`version`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `fos_user`
--
ALTER TABLE `fos_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
